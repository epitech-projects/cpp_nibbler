
//
// Window.cpp for nibbler in /home/gravie_j/projets/cpp_nibbler/opengl
//
// Made by Jean Gravier
// Login   gravie_j<gravie_j@epitech.net>
//
// Started on  Tue Apr  1 15:52:16 2014 Jean Gravier
// Last update Sun Apr  6 20:38:43 2014 Jeremy Peltier
//

#include <GL/freeglut.h>
#include <SOIL.h>
#include <list>
#include <cmath>
#include "opengl/Window.hpp"
#include "opengl/Camera.hpp"
#include "opengl/Vector3D.hpp"
#include "opengl/windowUtils.hpp"
#include "IWindow.hpp"
#include "Nibbler.hpp"

Window::Window(int x, int y)
  : _sizeX(x), _sizeY(y)
{
  this->initGlut();
  this->_camera = new Camera(Vector3D(0, 0, 1));
  this->_open = true;
  this->_key = NONE;
  this->_mode = OVERVIEW;
  this->loadTextures();
}

Window::~Window()
{
  delete this->_camera;
}

void		Window::initGlut() const
{
  int		temp = 0;

  glutInit(&temp, NULL);
  glutInitWindowPosition(-1, -1);
  glutInitWindowSize(800, 600);
  glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
  glutCreateWindow("Nibbler - OpenGL");
  glutDisplayFunc(displayFunc);
  glutReshapeFunc(reshapeFunc);
  glutIdleFunc(idleFunc);
  glutIgnoreKeyRepeat(1);
  glutKeyboardFunc(normalKeyFunc);
  glutSpecialFunc(keyDownFunc);
  glutSpecialUpFunc(keyUpFunc);
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_LIGHTING);
  glEnable(GL_LIGHT0);
  glEnable(GL_LIGHT1);
  glEnable(GL_LIGHT2);
  glEnable(GL_LIGHT3);
  glEnable(GL_TEXTURE_2D);
  glEnable(GL_BLEND);
  glEnable(GL_COLOR_MATERIAL);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
}

void		Window::drawSnake(Snake const& snake, FoodBlock const& food)
{
  this->_snake = snake;
  this->_food = food;
  this->_camera->animate(snake.front(), this->_mode, this->_sizeX, this->_sizeY);
  glutMainLoopEvent();
}

void		Window::loadTextures()
{
  std::string	path[TEXTURES_NB];

  path[0] = "opengl/textures/fence.jpg";
  path[1] = "opengl/textures/grass.jpg";
  path[2] = "opengl/textures/snake.jpg";
  path[3] = "opengl/textures/skyright.jpg";
  path[4] = "opengl/textures/skyfront.jpg";
  path[5] = "opengl/textures/skyleft.jpg";
  path[6] = "opengl/textures/skyback.jpg";
  path[7] = "opengl/textures/skytop.jpg";
  for (int i = 0; i < TEXTURES_NB; ++i)
    {
      this->_textures[i] = SOIL_load_OGL_texture(path[i].c_str(),
						 SOIL_LOAD_AUTO,
						 SOIL_CREATE_NEW_ID,
						 SOIL_FLAG_MIPMAPS |
						 SOIL_FLAG_INVERT_Y |
						 SOIL_FLAG_NTSC_SAFE_RGB |
						 SOIL_FLAG_COMPRESS_TO_DXT);
      if (!this->_textures[i])
	std::cerr << "SOIL loading error: " << SOIL_last_result() << std::endl;
    }
}

bool		Window::isOpen()
{
  return (this->_open);
}

void		Window::close()
{
  this->_open = false;
}

int		Window::getSizeX() const
{
  return (this->_sizeX);
}

int		Window::getSizeY() const
{
  return (this->_sizeY);
}

Direction	Window::getKey()
{
  Direction	key = this->_key;

  this->_key = NONE;
  return (key);
}

void		Window::setKey(Direction key)
{
  this->_key = key;
}

void		Window::setViewMode(ViewMode mode)
{
  this->_mode = mode;
}

ViewMode	Window::getViewMode() const
{
  return (this->_mode);
}

const GLuint	*Window::getTextures() const
{
  return (this->_textures);
}

const Snake	Window::getSnake() const
{
  return (this->_snake);
}

const FoodBlock	Window::getFood() const
{
  return (this->_food);
}

Camera		*Window::getCamera() const
{
  return (this->_camera);
}
