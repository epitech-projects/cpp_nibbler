//
// Scene.cpp for nibbler in /home/gravie_j/projets/cpp_nibbler/opengl
//
// Made by Jean Gravier
// Login   gravie_j<gravie_j@epitech.net>
//
// Started on  Wed Apr  2 11:15:24 2014 Jean Gravier
// Last update Sun Apr  6 03:50:37 2014 Jean Gravier
//

#include <GL/freeglut.h>
#include <list>
#include "Nibbler.hpp"
#include "opengl/Window.hpp"

#define WALL_HEIGHT 4.0

void		drawLight(float sizeX, float sizeY)
{
  float		ambientLight[3] = {0.3, 0.3, 0.3};
  float		diffuseLight[3] = {0.7, 0.7, 0.7};
  float		specularLight[3] = {0.9, 1.0, 1.0};
  float		posLight[4][4] = {
    {-sizeX, sizeY, 10.0, 1.0},
    {sizeX, sizeY, 10.0, 1.0},
    {-sizeX, -sizeY, 10.0, 1.0},
    {sizeX, -sizeY, 10.0, 1.0},
  };
  float		dirLight[4][3] = {
    {sizeX / 4, -sizeY / 4, -7.0},
    {-sizeX / 4, -sizeY / 4, -7.0},
    {sizeX / 4, sizeY / 4, -7.0},
    {-sizeX / 4, sizeY / 4, -7.0}
  };

  glMaterialfv(GL_FRONT, GL_SPECULAR, specularLight);
  glMateriali(GL_FRONT,GL_SHININESS, 64);
  for (int i = 0; i < 4; ++i)
    {
      glLightfv(GL_LIGHT0 + i, GL_POSITION, posLight[i]);
      glLightfv(GL_LIGHT0 + i, GL_SPOT_DIRECTION, dirLight[i]);
      glLightfv(GL_LIGHT0 + i, GL_SPECULAR, specularLight);
      glLightfv(GL_LIGHT0 + i, GL_DIFFUSE, diffuseLight);
      glLightfv(GL_LIGHT0 + i, GL_AMBIENT, ambientLight);
      glLightf(GL_LIGHT0 + i, GL_SPOT_EXPONENT, 10);
      glLightf(GL_LIGHT0 + i, GL_SPOT_CUTOFF, 70);
    }
}

void	drawFences(GLuint const textures[], float sizeX, float sizeY, ViewMode mode)
{
  float	texX;
  float	texY;

  texX = 0;
  texY = 0;
  glPushMatrix();
  glBindTexture(GL_TEXTURE_2D, textures[0]);
  for (int i = 0; i < 4; i++)
    {
      for (int z = 0; z < WALL_HEIGHT; ++z)
  	{
  	  for (float x = -sizeX / 2; x < sizeX / 2; ++x)
  	    {
  	      glBegin(GL_TRIANGLE_STRIP);
  	      glTexCoord2f(texX, texY);
  	      glVertex3f(x, sizeY / 2, z);
  	      glTexCoord2f(texX + (1 / (sizeX + 1)), texY);
  	      glVertex3f((x + 1), sizeY / 2, z);
  	      glTexCoord2f(texX, texY + (1 / (sizeY + 1)));
  	      glVertex3f(x, sizeY / 2, (z + 1));
  	      glTexCoord2f(texX + (1 / (sizeX + 1)), texY + (1 / (sizeY + 1)));
  	      glVertex3f((x + 1), sizeY / 2, (z + 1));
  	      glEnd();
	      texX += (1 / (sizeX + 1));
  	    }
	  texX = 0;
	  texY += (1 / (sizeY + 1));
  	}
      glRotated(90, 0, 0, 1);
      ++i;
      if (i == 1 && mode == THIRD_PERSON)
	glColor4f(1.0, 1.0, 1.0, 0.5);
      for (int z = 0; z < WALL_HEIGHT; ++z)
  	{
  	  for (float x = -sizeY / 2; x < sizeY / 2; ++x)
  	    {
  	      glBegin(GL_TRIANGLE_STRIP);
  	      glTexCoord2f(texX, texY);
  	      glVertex3f(x, sizeX / 2, z);
  	      glTexCoord2f(texX + (1 / (sizeY + 1)), texY);
  	      glVertex3f((x + 1), sizeX / 2, z);
  	      glTexCoord2f(texX, texY + (1 / (sizeX + 1)));
  	      glVertex3f(x, sizeX / 2, (z + 1));
  	      glTexCoord2f(texX + (1 / (sizeY + 1)), texY + (1 / (sizeX + 1)));
  	      glVertex3f((x + 1), sizeX / 2, (z + 1));
  	      glEnd();
	      texX += (1 / (sizeY + 1));
  	    }
	  texX = 0;
	  texY += (1 / (sizeX + 1));
  	}
      if (i == 3 && mode == THIRD_PERSON)
  	glColor4f(1.0, 1.0, 1.0, 1.0);
      texY = 0;
      glRotated(90, 0, 0, 1);
    }
  glPopMatrix();
}

void		drawSkybox(GLuint const textures[], int sizeX, int sizeY)
{
  glPushMatrix();
  for (int i = 0; i < 2; ++i)
    {
      glBindTexture(GL_TEXTURE_2D, textures[i + 4]);
      glBegin(GL_TRIANGLE_STRIP);
      glTexCoord2d(0, 0);
      glVertex3f(-sizeX - 100, sizeY + 100, -100);
      glTexCoord2d(0, 1);
      glVertex3f(-sizeX - 100, sizeY + 100, 100);
      glTexCoord2d(1, 0);
      glVertex3f(sizeX + 100, sizeY + 100, -100);
      glTexCoord2d(1, 1);
      glVertex3f(sizeX + 100, sizeY + 100, 100);
      glEnd();
      glRotated(180, 0, 0, 1);
    }
  glRotated(90, 0, 0, 1);
  for (int i = 0; i < 2; ++i)
    {
      glBindTexture(GL_TEXTURE_2D, textures[i + 4]);
      glBegin(GL_TRIANGLE_STRIP);
      glTexCoord2d(0, 0);
      glVertex3f(-sizeY - 100, sizeX + 100, -100);
      glTexCoord2d(0, 1);
      glVertex3f(-sizeY - 100, sizeX + 100, 100);
      glTexCoord2d(1, 0);
      glVertex3f(sizeY + 100, sizeX + 100, -100);
      glTexCoord2d(1, 1);
      glVertex3f(sizeY + 100, sizeX + 100, 100);
      glEnd();
      glRotated(180, 0, 0, 1);
    }
  glRotated(90, 0, 0, 1);
  glBindTexture(GL_TEXTURE_2D, textures[7]);
  glBegin(GL_TRIANGLE_STRIP);
  glTexCoord2d(0, 0);
  glVertex3f(-sizeX - 100, -sizeY - 100, -100);
  glTexCoord2d(1, 0);
  glVertex3f(sizeX + 100, -sizeY - 100, -100);
  glTexCoord2d(0, 1);
  glVertex3f(-sizeX - 100, sizeY + 100, -100);
  glTexCoord2d(1, 1);
  glVertex3f(sizeX + 100, sizeY + 100, -100);
  glEnd();
  glPopMatrix();
}

void			drawSnake(Window const *that)
{
  float			sizeX;
  float			sizeY;
  FoodBlock		food;
  Snake			snake;
  Snake::const_iterator	it;

  glDisable(GL_TEXTURE_2D);
  sizeX = that->getSizeX() / 2 - 0.5;
  sizeY = that->getSizeY() / 2 - 0.5;
  snake = that->getSnake();
  food = that->getFood();
  glColor3ub(230, 10, 10);
  for (it = snake.begin(); it != snake.end(); ++it)
    {
      glPushMatrix();
      glTranslatef((*it)->x - sizeX, (*it)->y - sizeY, 0.5f);
      if ((*it)->isHead)
	glColor3ub(103, 163, 0);
      else
	glColor3ub(144, 173, 95);
      glutSolidCube(1);
      glPopMatrix();
    }
  glPushMatrix();
  glTranslatef(food.x - sizeX, food.y - sizeY, 0.5f);
  glColor3ub(230, 39, 39);
  glutSolidSphere(0.5, 16, 16);
  glPopMatrix();
  glColor3d(1, 1, 1);
  glEnable(GL_TEXTURE_2D);
}

void		drawScene(Window const *that)
{
  float		texX;
  float		texY;
  float		sizeX;
  float		sizeY;
  GLuint const	*textures;

  texX = 0;
  texY = 0;
  sizeX = that->getSizeX();
  sizeY = that->getSizeY();
  textures = that->getTextures();
  drawLight(sizeX / 2, sizeY / 2);
  glNormal3d(0, 0, 1);
  glBindTexture(GL_TEXTURE_2D, textures[1]);
  for (float y = -sizeY / 2; y < sizeY / 2; ++y)
    {
      for (float x = -sizeX / 2; x < sizeX / 2; ++x)
  	{
  	  glBegin(GL_TRIANGLE_STRIP);
  	  glTexCoord2f(texX, texY);
  	  glVertex3f(x, y, 0);
  	  glTexCoord2f(texX + (1 / (sizeX + 1)), texY);
  	  glVertex3f(x + 1, y, 0);
  	  glTexCoord2f(texX, texY + (1 / (sizeY + 1)));
  	  glVertex3f(x, y + 1, 0);
  	  glTexCoord2f(texX + (1 / (sizeX + 1)), texY + (1 / (sizeY + 1)));
  	  glVertex3f(x + 1, y + 1, 0);
  	  glEnd();
	  texX += (1 / (sizeX + 1));
  	}
      texX = 0;
      texY += (1 / (sizeY + 1));
    }
  drawSnake(that);
  drawFences(that->getTextures(), sizeX, sizeY, that->getViewMode());
  drawSkybox(textures, sizeX / 2, sizeY / 2);
}
