//
// main.cpp for test in /home/gravie_j/projets/cpp_nibbler/opengl
//
// Made by Jean Gravier
// Login   gravie_j<gravie_j@epitech.net>
//
// Started on  Tue Apr  1 16:41:44 2014 Jean Gravier
// Last update Wed Apr  2 23:17:19 2014 Jean Gravier
//

#include <iostream>
#include <list>

#ifdef _WIN32
	#include <io.h>
	#include "dlfcn.h"
	#include "win-gettimeofday.h"
#else
	#include <unistd.h>
	#include <sys/time.h>
	#include <dlfcn.h>
#endif

#include "Nibbler.hpp"
#include "IWindow.hpp"
#include "opengl/windowUtils.hpp"
#include "opengl/Window.hpp"

IWindow		*initGraphics(int, int);

//int		main()
//{
//  IWindow	*window;
//  std::list<SnakeBlock *>test;
//  FoodBlock	test2;
//
//  window = initGraphics(10, 10);
//  while (1)
//    {
//      window->drawSnake(test, test2);
//    }
//  delete window;
//  return (0);
//}
