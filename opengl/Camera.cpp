//
// Camera.cpp for nibbler in /home/gravie_j/projets/cpp_nibbler/opengl
//
// Made by Jean Gravier
// Login   gravie_j<gravie_j@epitech.net>
//
// Started on  Tue Apr  1 22:06:58 2014 Jean Gravier
// Last update Sun Apr  6 00:33:42 2014 Jean Gravier
//

#define _USE_MATH_DEFINES
#include <cmath>
#include <iostream>
#include <GL/freeglut.h>
#include "opengl/Window.hpp"
#include "opengl/Vector3D.hpp"
#include "opengl/Camera.hpp"
#include "Nibbler.hpp"

Camera::Camera(Vector3D const& position)
{
  this->_position = position;
  this->_phi = 0.0f;
  this->_theta = 0.0f;
  this->vectorsFromAngles();
}

Camera::~Camera()
{

}

float		Camera::getTheta() const
{
  return (this->_theta);
}

void		Camera::setTheta(float theta)
{
  this->_theta = theta;
}

float		Camera::getPhi() const
{
  return (this->_phi);
}

void		Camera::setPhi(float phi)
{
  this->_phi = phi;
}

void		Camera::vectorsFromAngles()
{
  static const	Vector3D up(0, 0, 1);
  double	r_temp;

  if (this->_phi > 89)
    this->_phi = 89;
  else if (this->_phi < -89)
    this->_phi = -89;

  r_temp = cos(this->_phi * M_PI / 180);

  this->_forward.setZ(sin(this->_phi * M_PI / 180));
  this->_forward.setX(r_temp * cos(this->_theta * M_PI / 180));
  this->_forward.setY(r_temp * sin(this->_theta * M_PI / 180));

  this->_left = up.crossProduct(this->_forward);
  this->_left.normalize();

  this->_target = this->_position + this->_forward;
}

void	Camera::animate(SnakeBlock *head, ViewMode mode, int sizeX, int sizeY)
{
  if (mode == FIRST_PERSON)
    {
      this->_position.setZ(5.0);
      switch (head->direction)
      	{
      	case UP:
	  this->_position.setX(head->x - (sizeX / 2) + 0.5);
	  this->_position.setY(head->y - (sizeY / 2) + 5.0);
      	  this->_theta = -90.0;
      	  break;
      	case DOWN:
	  this->_position.setX(head->x - (sizeX / 2) + 0.5);
	  this->_position.setY(head->y - (sizeY / 2) - 5.0);
      	  this->_theta = 90.0;
      	  break;
      	case LEFT:
	  this->_position.setX(head->x - (sizeX / 2) + 5.0);
	  this->_position.setY(head->y - (sizeY / 2) + 0.5);
      	  this->_theta = 180.0;
      	  break;
      	case RIGHT:
	  this->_position.setX(head->x - (sizeX / 2) - 5.0);
	  this->_position.setY(head->y - (sizeY / 2) + 0.5);
      	  this->_theta = 0.0;
      	  break;
      	}
      this->vectorsFromAngles();
      this->_target.setZ(this->_target.getZ() - 0.5);
    }
  else if (mode == THIRD_PERSON)
    {
      this->_position.setX(head->x - sizeX / 2);
      this->_position.setY(head->y - sizeY / 2 - 15.0);
      this->_position.setZ(15.0);
      this->_target.setX(head->x - sizeX / 2);
      this->_target.setY(head->y - sizeY / 2);
      this->_target.setZ(0.5);
    }
  else
    {
      this->_position.setX(0.0);
      this->_position.setY(0.0);
      if (sizeX > sizeY)
	this->_position.setZ(sizeX + 5.0);
      else
	this->_position.setZ(sizeY + 15.0);
      this->_target.setX(0.0);
      this->_target.setY(0.0);
      this->_target.setZ(0.0);
    }
}

void	Camera::look(ViewMode mode)
{
  if (mode == FIRST_PERSON)
    gluLookAt(this->_position.getX(), this->_position.getY(), this->_position.getZ(),
	      this->_target.getX(), this->_target.getY(), this->_target.getZ(),
	      0.0f, 0.0f, 1.0f);
  else
    gluLookAt(this->_position.getX(), this->_position.getY(), this->_position.getZ(),
	      this->_target.getX(), this->_target.getY(), this->_target.getZ(),
	      0.0f, 1.0f, 0.0f);
}
