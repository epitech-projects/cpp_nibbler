//
// Vector3D.cpp for nibbler in /home/gravie_j/projets/cpp_nibbler/opengl
//
// Made by Jean Gravier
// Login   gravie_j<gravie_j@epitech.net>
//
// Started on  Tue Apr  1 22:57:05 2014 Jean Gravier
// Last update Wed Apr  2 02:21:05 2014 Jean Gravier
//

#include <cmath>
#include "opengl/Vector3D.hpp"

Vector3D::Vector3D()
  : _X(0.0f), _Y(0.0f), _Z(0.0f)
{

}

Vector3D::Vector3D(float x, float y, float z)
  : _X(x), _Y(y), _Z(z)
{

}

Vector3D::Vector3D(Vector3D const& vector)
  : _X(vector.getX()), _Y(vector.getY()), _Z(vector.getZ())
{

}

Vector3D::Vector3D(Vector3D const& src, Vector3D const& dest)
  : _X(dest.getX() - src.getX()),
    _Y(dest.getY() - src.getY()),
    _Z(dest.getZ() - src.getZ())
{

}

Vector3D::~Vector3D()
{

}
Vector3D	&Vector3D::operator=(Vector3D const& vector)
{
  this->_X = vector.getX();
  this->_Y = vector.getY();
  this->_Z = vector.getZ();
  return (*this);
}

Vector3D	&Vector3D::operator+=(Vector3D const& vector)
{
  this->_X += vector.getX();
  this->_Y += vector.getY();
  this->_Z += vector.getZ();
  return (*this);
}

Vector3D	Vector3D::operator+(Vector3D const& vector) const
{
  Vector3D	v;

  v.setX(this->_X + vector.getX());
  v.setY(this->_Y + vector.getY());
  v.setZ(this->_Z + vector.getZ());
  return (v);
}

Vector3D	&Vector3D::operator-=(Vector3D const& vector)
{
  this->_X -= vector.getX();
  this->_Y -= vector.getY();
  this->_Z -= vector.getZ();
  return (*this);
}

Vector3D	Vector3D::operator-(Vector3D const& vector) const
{
  Vector3D	v;

  v.setX(this->_X - vector.getX());
  v.setY(this->_Y - vector.getY());
  v.setZ(this->_Z - vector.getZ());
  return (v);
}

Vector3D	&Vector3D::operator*=(float const a)
{
  this->_X *= a;
  this->_Y *= a;
  this->_Z *= a;
  return (*this);
}

Vector3D	Vector3D::operator*(float const a) const
{
  Vector3D	v;

  v.setX(this->_X * a);
  v.setY(this->_Y * a);
  v.setZ(this->_Z * a);
  return (v);
}

Vector3D	&Vector3D::operator/=(float const a)
{
  this->_X *= a;
  this->_Y *= a;
  this->_Z *= a;
  return (*this);
}

Vector3D	Vector3D::operator/(float const a) const
{
  Vector3D	v;

  v.setX(this->_X / a);
  v.setY(this->_Y / a);
  v.setZ(this->_Z / a);
  return (v);
}

Vector3D	Vector3D::crossProduct(Vector3D const& vector) const
{
  Vector3D v;

  v.setX(this->_Y * vector.getZ() - this->_Z * vector.getY());
  v.setY(this->_Z * vector.getX() - this->_X * vector.getZ());
  v.setZ(this->_X * vector.getY() - this->_Y * vector.getX());
  return (v);
}

Vector3D	&Vector3D::normalize()
{
  (*this) /= this->length();
  return (*this);
}

float		Vector3D::length() const
{
  return (sqrt((this->_X * this->_X) +
	       (this->_Y * this->_Y) +
	       (this->_Z * this->_Z)));
}

Vector3D	operator*(float const a, Vector3D const& vector)
{
  Vector3D	v(vector);

  v *= a;
  return (v);
}

float		Vector3D::getX() const
{
  return (this->_X);
}

float		Vector3D::getY() const
{
  return (this->_Y);
}

float		Vector3D::getZ() const
{
  return (this->_Z);
}

void		Vector3D::setX(float x)
{
this->_X = x;
}

void		Vector3D::setY(float y)
{
this->_Y = y;
}

void		Vector3D::setZ(float z)
{
this->_Z = z;
}
