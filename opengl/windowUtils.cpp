//
// initGraphics.cpp for nibbler in /home/gravie_j/projets/cpp_nibbler/opengl
//
// Made by Jean Gravier
// Login   gravie_j<gravie_j@epitech.net>
//
// Started on  Tue Apr  1 15:51:08 2014 Jean Gravier
// Last update Sun Apr  6 20:32:18 2014 Jean Gravier
//

#include <GL/freeglut.h>
#include <cmath>
#include "opengl/Window.hpp"
#include "opengl/Camera.hpp"
#include "opengl/windowUtils.hpp"
#include "IWindow.hpp"

Window	*that = NULL;

extern "C"
{
  __declspec(dllexport) IWindow		*initGraphics(int x, int y)
  {
    Window	*window = new Window(x, y);

    that = window;
    return (window);
  }
}

void		displayFunc()
{
  Camera	*camera = that->getCamera();

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glLoadIdentity();
  glMatrixMode(GL_MODELVIEW);

  camera->look(that->getViewMode());

  drawScene(that);

  glFlush();
  glutSwapBuffers();
  glutPostRedisplay();
}

void		reshapeFunc(int w, int h)
{
  float		ratio;

  if(h == 0)
    h = 1;
  ratio = 1.0 * w / h;

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glViewport(0, 0, w, h);
  gluPerspective(45, ratio, 1, 5000);
  glMatrixMode(GL_MODELVIEW);
}

void			normalKeyFunc(unsigned char key, int x, int y)
{
  (void)x;
  (void)y;
  switch (key)
    {
    case 'z':
      that->setKey(DOWN);
      break;
    case 's':
      that->setKey(UP);
      break;
    case 'q':
      that->setKey(RIGHT);
      break;
    case 'd':
      that->setKey(LEFT);
      break;
    case 27:
      that->close();
      break;
    }
}

void			keyDownFunc(int key, int x, int y)
{
  (void)x;
  (void)y;
  switch (key)
    {
    case GLUT_KEY_UP:
      that->setKey(DOWN);
      break;
    case GLUT_KEY_DOWN:
      that->setKey(UP);
      break;
    case GLUT_KEY_LEFT:
      that->setKey(RIGHT);
      break;
    case GLUT_KEY_RIGHT:
      that->setKey(LEFT);
      break;
    case GLUT_KEY_F1:
      that->setViewMode(OVERVIEW);
      break;
    case GLUT_KEY_F2:
      that->setViewMode(THIRD_PERSON);
      break;
    case GLUT_KEY_F3:
      that->setViewMode(FIRST_PERSON);
      break;
    }
}

void			keyUpFunc(int key, int x, int y)
{
  (void)x;
  (void)y;
  if (key == GLUT_KEY_UP ||
      key == GLUT_KEY_DOWN ||
      key == GLUT_KEY_LEFT ||
      key == GLUT_KEY_RIGHT)
    that->setKey(NONE);
}

void		idleFunc()
{
  displayFunc();
}
