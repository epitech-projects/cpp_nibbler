//
// sfml.cpp for SFML in /home/peltie_j/Desktop/Epitech/Tek2/c++/nibbler/cpp_nibbler/sfml
//
// Made by Jeremy Peltier
// Login   <peltie_j@epitech.net>
//
// Started on  Tue Apr  1 14:31:22 2014 Jeremy Peltier
// Last Wed update Apr  2 09:58:56 2014 Jeremy Peltier
//

#include	"sfml/Sfml.hpp"

SFML::SFML(int width, int height)
{
  this->window.create(sf::VideoMode(width * 30, height * 30, 32), "Nibbler");
  this->window.clear();
  this->window.display();
  if (!this->head.loadFromFile("sfml/sprites/head.png"))
    std::cout << "Can't load file sprites/head.png" << std::endl;
  if (!this->body.loadFromFile("sfml/sprites/body.png"))
    std::cout << "Can't load file sprites/body.png" << std::endl;
  if (!this->fruit.loadFromFile("sfml/sprites/fruit.png"))
    std::cout << "Can't load file sprites/fruit.png" << std::endl;
}

SFML::~SFML()
{
}

void	SFML::drawSnake(std::list<SnakeBlock *> const &snake, FoodBlock const &food)
{
  this->window.clear();

  sf::Sprite		fruitSprite;
  fruitSprite.setTexture(this->fruit);
  fruitSprite.setPosition((food.x * 30), (food.y * 30));
  this->window.draw(fruitSprite);

  for (std::list<SnakeBlock *>::const_iterator it = snake.begin(); it != snake.end(); ++it)
    {
      sf::Sprite snakeSprite;
      snakeSprite.setPosition(((*it)->x * 30), ((*it)->y * 30));
      if ((*it)->isHead)
	snakeSprite.setTexture(this->head);
      else
	snakeSprite.setTexture(this->body);
      this->window.draw(snakeSprite);
    }

  this->window.display();
}

Direction	SFML::getKey()
{
  while (this->window.pollEvent(this->event))
    {
      if (this->event.type == sf::Event::Closed || sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
	this->window.close();
      if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
	return LEFT;
      else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
	return RIGHT;
    }
  return NONE;
}

bool		SFML::isOpen()
{
  return this->window.isOpen();
}

extern "C" __declspec(dllexport) IWindow	*initGraphics(int width, int height)
{
  SFML	*window = new SFML(width, height);

  return (window);
}
