//
// core.hpp for nibbler in /home/hillai_a/Rendus/Cpp/cpp_nibbler/core
//
// Made by Remi Hillairet
// Login   <hillai_a@epitech.net>
//
// Started on  Wed Mar 19 14:58:12 2014 Remi Hillairet
// Last update Sun Apr  6 21:12:51 2014 Remi Hillairet
//

#ifndef __CORE_HPP__

# define __CORE_HPP__

# define FPS 15

#include <list>

#ifdef _WIN32
	#include <io.h>
	#include "dlfcn.h"
	#include "win-gettimeofday.h"
#else
	#include <unistd.h>
	#include <sys/time.h>
	#include <dlfcn.h>
#endif

#include <stdlib.h>
#include "Nibbler.hpp"
#include "IWindow.hpp"

class Core
{
public:
  Core(int, int, const std::string &);
  virtual ~Core();
  void initDynamicLibrary();
  void play();
  void moveFood();
  int detectCollision();
  void moveSnake();
  void changeDirection();
  void updateTimer();
  void limitFrames();

private:
  int	width;
  int	height;
  std::list<SnakeBlock*> snake;
  FoodBlock food;
  bool addSegment;
  Direction key;
  IWindow *window;
  std::string libso;
  void *handle;
  struct timeval lastTime;
  struct timeval currentTime;
};

#endif /* __CORE_HPP__ */
