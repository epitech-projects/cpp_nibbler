//
// Window.hh for nibbler in /home/gravie_j/projets/cpp_nibbler/opengl
//
// Made by Jean Gravier
// Login   gravie_j<gravie_j@epitech.net>
//
// Started on  Tue Apr  1 00:00:01 2014 Jean Gravier
// Last update Sun Apr  6 20:38:04 2014 Jeremy Peltier
//

#ifndef WINDOW_HH_
# define WINDOW_HH_

# include <iostream>
# include <list>
# include "IWindow.hpp"
# include "Nibbler.hpp"

# define TEXTURES_NB 8

class Camera;

typedef std::list<SnakeBlock *>	Snake;

enum ViewMode {
  FIRST_PERSON,
  THIRD_PERSON,
  OVERVIEW
};

class				Window: public IWindow
{
public:
  Window(int, int);
  ~Window();

  void				drawSnake(Snake const&, FoodBlock const&);
  void				loadTextures();

  void				look();

  bool				isOpen();
  void				close();

  int				getSizeX() const;
  int				getSizeY() const;
  Direction			getKey();
  void				setKey(Direction);
  ViewMode			getViewMode() const;
  void				setViewMode(ViewMode);
  GLuint const			*getTextures() const;
  Snake const			getSnake() const;
  FoodBlock const		getFood() const;
  Camera			*getCamera() const;

private:
  void				initGlut() const;
  Camera			*_camera;
  int				_sizeX;
  int				_sizeY;
  Direction			_key;
  ViewMode			_mode;
  GLuint			_textures[TEXTURES_NB];
  Snake				_snake;
  FoodBlock			_food;
  bool				_open;
};

#endif /* !WINDOW_HH_ */
