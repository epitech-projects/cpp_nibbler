//
// windowUtils.hpp for nibbler in /home/gravie_j/projets/cpp_nibbler/opengl
//
// Made by Jean Gravier
// Login   gravie_j<gravie_j@epitech.net>
//
// Started on  Tue Apr  1 16:56:48 2014 Jean Gravier
// Last update Sat Apr  5 19:17:59 2014 Jean Gravier
//

#ifndef WINDOWUTILS_H_
# define WINDOWUTILS_H_

#include <GL/freeglut.h>
#include "Window.hpp"

void		reshapeFunc(int, int);
void		displayFunc();
void		idleFunc();
void		normalKeyFunc(unsigned char, int, int);
void		keyDownFunc(int, int, int);
void		keyUpFunc(int, int, int);
void		mouseFunc(int, int);
void		drawScene(Window const*);

#endif /* !WINDOWUTILS_H_ */
