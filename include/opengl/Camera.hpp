//
// Camera.hpp for nibbler in /home/gravie_j/projets/cpp_nibbler/opengl
//
// Made by Jean Gravier
// Login   gravie_j<gravie_j@epitech.net>
//
// Started on  Tue Apr  1 22:08:12 2014 Jean Gravier
// Last update Sun Apr  6 00:24:17 2014 Jean Gravier
//

#ifndef CAMERA_HPP_
# define CAMERA_HPP_

# include <GL/freeglut.h>
# include "Vector3D.hpp"
# include "Window.hpp"

class		Camera
{
public:
  Camera(Vector3D const& position);
  virtual ~Camera();

  void		vectorsFromAngles();

  void		animate(SnakeBlock *, ViewMode, int, int);
  void		look(ViewMode);

  float		getTheta() const;
  void		setTheta(float);

  float		getPhi() const;
  void		setPhi(float);

private:
  Vector3D	_position;
  Vector3D	_target;
  Vector3D	_forward;
  Vector3D	_left;

  float		_theta;
  float		_phi;
};

#endif /* !CAMERA_HPP_ */
