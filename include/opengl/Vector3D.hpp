//
// Vector3D.hpp for nibbler in /home/gravie_j/projets/cpp_nibbler/opengl
//
// Made by Jean Gravier
// Login   gravie_j<gravie_j@epitech.net>
//
// Started on  Tue Apr  1 22:57:42 2014 Jean Gravier
// Last update Wed Apr  2 01:08:52 2014 Jean Gravier
//

#ifndef VECTOR3D_HH_
#define VECTOR3D_HH_

class		Vector3D
{
public:
  Vector3D();
  Vector3D(float, float, float);
  Vector3D(Vector3D const&);
  Vector3D(Vector3D const&, Vector3D const&);
  ~Vector3D();

  Vector3D	&operator=(Vector3D const&);

  Vector3D	&operator+=(Vector3D const&);
  Vector3D	operator+(Vector3D const&) const;

  Vector3D	&operator-=(Vector3D const&);
  Vector3D	operator-(Vector3D const&) const;

  Vector3D	&operator*=(const float);
  Vector3D	operator*(const float) const;

  Vector3D	&operator/=(const float);
  Vector3D	operator/(const float) const;

  Vector3D	crossProduct(Vector3D const&) const;
  Vector3D	&normalize();
  float		length() const;

  float		getX() const;
  float		getY() const;
  float		getZ() const;

  void		setX(float x);
  void		setY(float y);
  void		setZ(float z);

private:
  float		_X;
  float		_Y;
  float		_Z;
};

#endif /* VECTOR3D_HH_ */
