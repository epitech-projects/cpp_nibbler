//
// WindowNcurses.hpp for nibbler in /home/hillai_a/Rendus/Cpp/cpp_nibbler/ncurses
//
// Made by Remi Hillairet
// Login   <hillai_a@epitech.net>
//
// Started on  Tue Apr  1 14:16:44 2014 Remi Hillairet
// Last update Sun Apr  6 20:38:16 2014 Jeremy Peltier
//

#ifndef __WINDOW_NCURSES_H__

# define __WINDOW_NCURSES_H__

# include <ncurses.h>
# include <list>
# include "Nibbler.hpp"
# include "IWindow.hpp"

class WindowNcurses : public IWindow
{
public:
  WindowNcurses(int, int);
  void drawSnake(std::list<SnakeBlock *> const &, FoodBlock const &);
  Direction getKey();
  bool isOpen();
  virtual ~WindowNcurses();

private:
  int width;
  int height;
  bool open;
};

extern "C" IWindow *initGraphics(int, int);

#endif /* !__WINDOW_NCURSES_H__ */
