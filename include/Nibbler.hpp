//
// nibbler.hpp for nibbler in /home/hillai_a/Rendus/Cpp/cpp_nibbler
//
// Made by Remi Hillairet
// Login   <hillai_a@epitech.net>
//
// Started on  Tue Mar 18 11:23:13 2014 Remi Hillairet
// Last update Wed Apr  2 10:44:59 2014 Jeremy Peltier
//

#ifndef __NIBBLER_HPP__

# define __NIBBLER_HPP__

# include <iostream>
# include <sstream>
# include <string>

enum Direction
  {
    UP,
    RIGHT,
    DOWN,
    LEFT,
    NONE
  };

struct SnakeBlock
{
  bool		isHead;
  int		direction;
  int		x;
  int		y;
};

struct FoodBlock
{
  int		x;
  int		y;
};

#endif /* __NIBBLER_HPP__ */
