//
// IWindow.hpp for nibbler in /home/gravie_j/projets/cpp_nibbler
//
// Made by Jean Gravier
// Login   gravie_j<gravie_j@epitech.net>
//
// Started on  Tue Apr  1 14:10:45 2014 Jean Gravier
// Last update Sun Apr  6 20:37:38 2014 Jeremy Peltier
//

#ifndef IWINDOW_HH_
# define IWINDOW_HH_

# include <list>
# include "Nibbler.hpp"

class			IWindow
{
public:
  virtual ~IWindow() {};
  virtual void		drawSnake(std::list<SnakeBlock *> const&, FoodBlock const&) = 0;
  virtual Direction	getKey() = 0;
  virtual bool		isOpen() = 0;
};

#endif /* !IWINDOW_HH_ */
