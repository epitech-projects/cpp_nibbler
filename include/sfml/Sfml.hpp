//
// sfml.hpp for SFML in /home/peltie_j/Desktop/Epitech/Tek2/c++/nibbler/cpp_nibbler/includes/sfml
//
// Made by Jeremy Peltier
// Login   <peltie_j@epitech.net>
//
// Started on  Tue Apr  1 14:31:42 2014 Jeremy Peltier
// Last update Sun Apr  6 20:48:37 2014 Jeremy Peltier
//

#ifndef		SFML_HPP_
# define	SFML_HPP_

# include	<SFML/Graphics.hpp>
# include	<SFML/Window.hpp>
# include	<SFML/System.hpp>
# include	<SFML/Audio.hpp>
# include	"IWindow.hpp"
# include	"Nibbler.hpp"

class		SFML : public IWindow
{
public:
  SFML(int, int);
  virtual ~SFML();
  void		drawSnake(std::list<SnakeBlock *> const &, FoodBlock const &);
  Direction	getKey();
  bool		isOpen();

private:
  sf::RenderWindow	window;
  sf::Event		event;
  sf::Texture		head;
  sf::Texture		body;
  sf::Texture		fruit;
};

extern "C" __declspec(dllexport) IWindow	*initGraphics(int, int);

#endif		/* SFML_HPP_ */
