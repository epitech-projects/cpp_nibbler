//
// main.cpp for nibbler in /home/peltie_j/Documents/Epitech/Tek2/c++/nibbler/cpp_nibbler
//
// Made by Jeremy Peltier
// Login   <peltie_j@epitech.net>
//
// Started on  Mon Mar 17 14:00:41 2014 Jeremy Peltier
// Last update Sun Apr  6 21:13:08 2014 Remi Hillairet
//

#include "Nibbler.hpp"
#include "core/Core.hpp"

int	main(int ac, char **av, char **env)
{
  int	width;
  int	height;
  std::stringstream stringInt;
  std::string libso;

  if (env[0] == NULL)
    {
      std::cout << "nibbler : can't launch ./nibbler without environement" << std::endl;
      return (1);
    }

  if (ac == 4)
    {
      stringInt << av[1];
      stringInt >> width;
      stringInt.clear();
      stringInt << av[2];
      stringInt >> height;
      libso = av[3];
      if (width < 10 || height < 10)
	{
	  std::cout << "nibbler : width and height have to be greater than 10" << std::endl;
	  return 0;
	}
      if (width > 150 || height > 55)
	{
	  std::cout << "nibbler : width have to be lower than 150 and height have to be lower than 55" << std::endl;
	  return 0;
	}
	  Core nibbler(width, height, libso);
      nibbler.play();
      std::cout << "nibbler : Goodbye !" << std::endl;
    }
  else
    std::cout << "USAGE : ./nibbler width height lib.so" << std::endl;
  return 0;
}
