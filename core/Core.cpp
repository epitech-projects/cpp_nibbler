//
// core.cpp for nibbler in /home/hillai_a/Rendus/Cpp/cpp_nibbler/core
//
// Made by Remi Hillairet
// Login   <hillai_a@epitech.net>
//
// Started on  Wed Mar 19 14:57:22 2014 Remi Hillairet
// Last update Sun Apr  6 21:14:04 2014 Remi Hillairet
//

#include "core/Core.hpp"

#include <chrono>
#include <thread>

Core::Core(int _width, int _height, const std::string & _libso)
{
  SnakeBlock *head;
  SnakeBlock *block;

  width = _width;
  height = _height;
  libso = _libso;
  addSegment = 0;
  head = new SnakeBlock;
  head->isHead = true;
  head->direction = RIGHT;
  head->x = (width / 2) + 2;
  head->y = height / 2;
  snake.push_back(head);
  for (int i = 1 ; i > -2 ; --i)
    {
      block = new SnakeBlock;
      block->isHead = false;
      block->direction = RIGHT;
      block->x = (width / 2) + i;
      block->y = height / 2;
      snake.push_back(block);
    }
  srand((unsigned int) time(NULL));
  moveFood();
  initDynamicLibrary();
}

static bool deleteAllSnakeBlock(SnakeBlock * block)
{
  delete block;
  return true;
}

void Core::initDynamicLibrary()
{
  IWindow *(*initGraphics)(int, int);
  const char *dlsym_error;

  handle = dlopen(libso.c_str(), RTLD_LAZY);
  if (!handle)
    {
      std::cerr << "Cannot open library: " << dlerror() << std::endl;
      return ;
    }
  dlerror();
  *(void **)(&initGraphics) = dlsym(handle, "initGraphics");
  dlsym_error = dlerror();
  if (dlsym_error)
    {
      std::cerr << "Cannot load symbol initGraphics: " << dlsym_error << std::endl;
      dlclose(handle);
      handle = NULL;
      return ;
    }
  window = (*initGraphics)(width, height);
  if (!window)
    std::cerr << "nibbler : Can't open window" << std::endl;
}

void Core::changeDirection()
{
  int currentDirection;

  currentDirection = snake.front()->direction;
  if (key == LEFT)
    {
      if (currentDirection == UP)
	snake.front()->direction = LEFT;
      else
	snake.front()->direction = snake.front()->direction - 1;
    }
  else if (key == RIGHT)
    {
      if (currentDirection == LEFT)
	snake.front()->direction = UP;
      else
	snake.front()->direction = snake.front()->direction + 1;
    }
  key = NONE;
}

void Core::play()
{
  if ((!handle) || (!window))
    return ;
  while (window->isOpen())
    {
      updateTimer();
      window->drawSnake(snake, food);
      key = window->getKey();
      changeDirection();
      if (detectCollision() == -1)
	return ;
      moveSnake();
      limitFrames();
    }
  delete window;
}

void Core::moveFood()
{
  food.x = rand() % width;
  food.y = rand() % height;
}

int Core::detectCollision()
{
  if (snake.front()->x < 0 || snake.front()->x > (width - 1)
      || snake.front()->y > (height - 1) || snake.front()->y < 0)
    {
      delete window;
      std::cout << "nibbler : GAME OVER !!!" << std::endl;
      return -1;
    }
  else if (snake.front()->x == food.x && snake.front()->y == food.y)
    {
      addSegment = 1;
      moveFood();
    }
  for (std::list<SnakeBlock*>::iterator it = snake.begin() ; it != snake.end() ; ++it)
    {
      if (!(*it)->isHead && snake.front()->y == (*it)->y && snake.front()->x == (*it)->x)
	{
	  delete window;
	  std::cout << "nibbler : GAME OVER !!!" << std::endl;
	  return -1;
	}
    }
  return 0;
}

void Core::moveSnake()
{
  std::list<SnakeBlock*>::reverse_iterator it1;
  std::list<SnakeBlock*>::reverse_iterator it2;

  if (addSegment)
    {
      SnakeBlock *block = new SnakeBlock;
      block->isHead = 0;
      block->direction = snake.back()->direction;
      block->x = snake.back()->x;
      block->y = snake.back()->y;
      snake.push_back(block);
      addSegment = 0;
    }
  it1 = snake.rbegin();
  ++it1;
  it2 = snake.rbegin();
  while (it1 != snake.rend())
    {
      (*it2)->x = (*it1)->x;
      (*it2)->y = (*it1)->y;
      ++it1;
      ++it2;
    }
  if (snake.front()->direction == UP)
    snake.front()->y -=1;
  else if (snake.front()->direction == DOWN)
    snake.front()->y +=1;
  else if (snake.front()->direction == LEFT)
    snake.front()->x -=1;
  else
    snake.front()->x +=1;
}

Core::~Core()
{
  if (handle)
    dlclose(handle);
  snake.remove_if(deleteAllSnakeBlock);
}

void			Core::updateTimer()
{
  gettimeofday(&lastTime, NULL);
}

void			Core::limitFrames()
{
  struct timeval	time;

  gettimeofday(&currentTime, NULL);
  timersub(&currentTime, &lastTime, &time);

  if (time.tv_usec <= 1000000 / FPS)
	  std::this_thread::sleep_for(std::chrono::microseconds(1000000 / FPS - (time.tv_usec)));
}
