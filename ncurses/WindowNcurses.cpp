//
// WindowNcurses.cpp for nibbler in /home/hillai_a/Rendus/Cpp/cpp_nibbler/ncurses
//
// Made by Remi Hillairet
// Login   <hillai_a@epitech.net>
//
// Started on  Tue Apr  1 14:16:57 2014 Remi Hillairet
// Last update Sun Apr  6 21:12:22 2014 Remi Hillairet
//

#include "ncurses/WindowNcurses.hpp"

WindowNcurses::WindowNcurses(int _width, int _height)
{
  width = _width;
  height = _height;
  open = 1;
  initscr();
  curs_set(0);
  noecho();
  nodelay(stdscr, TRUE);
  start_color();
  keypad(stdscr, TRUE);
  init_pair(1, COLOR_BLACK, COLOR_WHITE);
  init_pair(2, COLOR_RED, COLOR_RED);
  init_pair(3, COLOR_YELLOW, COLOR_YELLOW);
  init_pair(4, COLOR_BLUE, COLOR_BLUE);
  attron(COLOR_PAIR(1));
}

WindowNcurses::~WindowNcurses()
{
  endwin();
}

void WindowNcurses::drawSnake(std::list<SnakeBlock *> const & snake, FoodBlock const & food)
{
  attron(COLOR_PAIR(1));
  clear();
  for (int y = 0 ; y < height ; ++y)
    {
      move(y, 0);
      for (int x = 0 ; x < width ; ++x)
	{
	  addch(' ');
	}
    }
  for (std::list<SnakeBlock*>::const_iterator it = snake.begin() ; it != snake.end() ; ++it)
    {
      move((*it)->y, (*it)->x);
      if ((*it)->isHead == 1)
	{
	  attron(COLOR_PAIR(3));
	  addch(' ');
	}
      else
	{
	  attron(COLOR_PAIR(4));
	  addch(' ');
	}
    }
  attron(COLOR_PAIR(2));
  move(food.y, food.x);
  addch(' ');
  refresh();
}

Direction WindowNcurses::getKey()
{
  int c;

  c = getch();
  if (c == KEY_RIGHT)
    return RIGHT;
  else if (c == KEY_LEFT)
    return LEFT;
  else if (c == 27)
    open = 0;
  return NONE;
}

bool WindowNcurses::isOpen()
{
  return open;
}

extern "C" IWindow *initGraphics(int _width, int _height)
{
  IWindow *window = new WindowNcurses(_width, _height);

  return window;
}
